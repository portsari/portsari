package fi.ayyy.portsari.bukkit
import fi.ayyy.portsari.Portsari
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent

class PortsariListener implements Listener {

    private PortsariPlugin plugin

    PortsariListener(PortsariPlugin plugin) {
        this.plugin = plugin
    }

    @EventHandler(priority = EventPriority.MONITOR)
    void onJoin(PlayerJoinEvent event) {
        Portsari.getInstance().handleLogin(plugin.wrapPlayer(event.getPlayer()).get())
    }
}
