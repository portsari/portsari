package fi.ayyy.portsari.bukkit
import fi.ayyy.portsari.api.LocalConfiguration
import org.bukkit.configuration.file.FileConfiguration

class BukkitConfiguration extends LocalConfiguration {

    private FileConfiguration config
    private PortsariPlugin plugin

    BukkitConfiguration(FileConfiguration config, PortsariPlugin plugin) {
        this.config = config
        this.plugin = plugin
    }

    @Override void load() {
        API_URL = config.getString("api.url")
        API_VERSION = config.getInt("api.version")

        SERVER_TOKEN = config.getString("server-token")
        IGNORED_REASONS = config.getIntegerList("ignored-reasons")
    }
}
