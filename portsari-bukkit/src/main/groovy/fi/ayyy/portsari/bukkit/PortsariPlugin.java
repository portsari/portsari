package fi.ayyy.portsari.bukkit;

import com.google.common.base.Optional;
import fi.ayyy.portsari.Portsari;
import fi.ayyy.portsari.api.event.CommandEvent;
import fi.ayyy.portsari.util.ClasspathHacker;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class PortsariPlugin extends JavaPlugin implements CommandExecutor {

    private BukkitServerInterface server;
    private BukkitConfiguration config;

    @Override
    public void onEnable() {
        try { // Groovy on paska, Bukkit paskempi ja laiskuus #itworks. Valituksia otetaan vastaan perjantaisin 7:59-8:00
            saveResource("groovy.jar", true);
            ClasspathHacker.addFile(new File(getDataFolder(), "groovy.jar"));
        } catch (IllegalArgumentException | IOException e) {
            e.printStackTrace();
            Bukkit.getPluginManager().disablePlugin(this);
        }

        Portsari portsari = Portsari.getInstance();

        loadConfig();

        server = new BukkitServerInterface(getServer(), this);
        if (portsari.getPlatformManager().register(server)) {
            getServer().getPluginManager().registerEvents(new PortsariListener(this), this);
        }
    }

    @Override
    public void onDisable() {
        Portsari.getInstance().getPlatformManager().unregister();
    }

    /**
     * Loads the server configuration
     */
    private void loadConfig() {
        saveDefaultConfig();

        config = new BukkitConfiguration(getConfig(), this);
        config.load();
    }

    /**
     * Reloads the server configuration
     */
    public void reloadConfiguration() {
        reloadConfig();

        config = new BukkitConfiguration(getConfig(), this);
        config.load();
    }

    /**
     * Gets the servers implementation of LocalConfiguration
     *
     * @return the Configuration
     */
    public BukkitConfiguration getLocalConfig() {
        return config;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length > 0) {
            label = args[0];
            args = Arrays.copyOfRange(args, 1, args.length);
        }


        CommandEvent event = new CommandEvent(wrapCommandSender(sender), label, args);
        Portsari.getInstance().getEventBus().post(event);

        return true;
    }

    /**
     * Used to wrap a Bukkit Player as a LocalPlayer.
     *
     * @param player a Player
     * @return a wrapper Player
     */
    public Optional<BukkitPlayer> wrapPlayer(OfflinePlayer player) {
        return Optional.fromNullable(player == null ?  null : new BukkitPlayer(player));
    }

    /**
     * Used to wrap a Bukkit CommandSender as a Actor.
     *
     * @param sender a Sender
     * @return a wrapper CommandSender
     */
    public BukkitCommandSender wrapCommandSender(CommandSender sender) {
        return new BukkitCommandSender(sender);
    }

}
