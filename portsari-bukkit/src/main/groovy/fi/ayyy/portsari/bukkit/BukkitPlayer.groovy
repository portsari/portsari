package fi.ayyy.portsari.bukkit
import fi.ayyy.portsari.api.LocalPlayer
import fi.ayyy.portsari.util.localisation.Msg
import org.bukkit.OfflinePlayer
import org.bukkit.entity.Player

class BukkitPlayer implements LocalPlayer {

    private OfflinePlayer player

    BukkitPlayer(OfflinePlayer player) {
        this.player = player
    }

    @Override void print(String message) {
        player.getPlayer()?.sendMessage(message)
    }

    @Override void print(Msg msg) { print(msg.f()) }

    @Override UUID getUniqueId() { player.getUniqueId() }

    @Override String getName() { player.getName() }

    @Override boolean hasPermission(String permission) {
        if (player instanceof Player) {
            return player.hasPermission(permission)
        }

        return false
    }

    @Override boolean isOp() { player.isOp() }

    @Override boolean isOnline() { player.isOnline() }

}
