package fi.ayyy.portsari.bukkit

import com.google.common.base.Optional
import fi.ayyy.portsari.api.Actor
import fi.ayyy.portsari.api.LocalConfiguration
import fi.ayyy.portsari.api.LocalPlayer
import fi.ayyy.portsari.api.ServerInterface
import org.bukkit.Bukkit
import org.bukkit.OfflinePlayer
import org.bukkit.Server

import static com.google.common.base.Preconditions.checkNotNull

class BukkitServerInterface implements ServerInterface {

    private Server server

    private PortsariPlugin plugin

    BukkitServerInterface(Server server, PortsariPlugin plugin) {
        checkNotNull(server)
        checkNotNull(plugin)

        this.server = server
        this.plugin = plugin
    }

    @Override Actor getConsole() { plugin.wrapCommandSender(Bukkit.getConsoleSender()) }

    @Override Optional<LocalPlayer> findPlayer(String name) {
        OfflinePlayer player = server.getOfflinePlayer(name)

        if (player.hasPlayedBefore()) {
            return plugin.wrapPlayer(player)
        }

        return Optional.absent()
    }

    @Override void disable() { server.getPluginManager().disablePlugin(plugin) }

    @Override void reload() { plugin.reloadConfiguration() }

    @Override LocalConfiguration getConfiguration() { plugin.getLocalConfig() }

    @Override boolean isOnlineMode() { server.getOnlineMode() }

    @Override String getServerName() { server.getName() }

    @Override String getServerVersion() { server.getVersion() }

    @Override void broadcast(String msg) { server.broadcastMessage(msg) }

    @Override void broadcast(String msg, String permission) { server.broadcast(msg, permission) }

}
