package fi.ayyy.portsari.bukkit
import fi.ayyy.portsari.api.Actor
import fi.ayyy.portsari.util.localisation.Msg
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class BukkitCommandSender implements Actor {

    private CommandSender sender

    BukkitCommandSender(CommandSender sender) {
        this.sender = sender
    }

    @Override void print(String msg) { sender.sendMessage(msg) }

    @Override void print(Msg msg) { sender.sendMessage(msg.f()) }

    @Override boolean isPlayer() { sender instanceof Player }

    @Override String getName() { sender.getName() }

    @Override boolean hasPermission(String permission) { sender.hasPermission(permission) }

}
