package fi.ayyy.portsari.gson

class Reason {

    int id
    def reason

    @Override String toString() { reason }

}
