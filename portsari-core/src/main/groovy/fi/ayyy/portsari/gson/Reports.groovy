package fi.ayyy.portsari.gson
import fi.ayyy.portsari.Portsari

class Reports {

    List<Report> reports

    boolean hasReports() { reports.size() > 0 }

    int count() { reports.size() }

    void removeIgnored() {
        def ignored = Portsari.getInstance().getConfig().getIgnoredReasons()

        reports.removeAll { ignored.contains(it.reason.id) }
    }
}