package fi.ayyy.portsari.gson

class Report {

    int id
    Date created_at
    Reason reason
    def info

    Player reporter
    Player player
    Server server

}
