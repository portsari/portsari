package fi.ayyy.portsari.http
import com.squareup.okhttp.Interceptor
import com.squareup.okhttp.Response
import fi.ayyy.portsari.Portsari
import fi.ayyy.portsari.api.Actor
import fi.ayyy.portsari.command.CommandContext
import fi.ayyy.portsari.gson.Error
import groovy.util.logging.Log

import static fi.ayyy.portsari.util.localisation.Msg.REPORT_ERROR

@Log
class InvalidResponseInterceptor implements Interceptor {

    @Override Response intercept(Interceptor.Chain chain) throws IOException {
        Response response = chain.proceed(chain.request())

        Actor actor = getActor(chain.request().tag()) // Dirty way to get the Actor, as there's no way to pass custom data with OkHttp (?)
        if (response.code() == 400) {
            def error = Portsari.GSON.fromJson(response.body().string(), Error.class)

            actor.print(error.getMessage())
        } else if (!response.isSuccessful()) {
            actor.print(REPORT_ERROR)
            log.info("HTTP Status Code: " + response.code())
        }

        return response
    }

    private def getActor(Object tag) {
        if (tag instanceof CommandContext) {
            ((CommandContext) tag).getActor()
        } else {
            Portsari.getInstance().getServer().getConsole()
        }
    }
}
