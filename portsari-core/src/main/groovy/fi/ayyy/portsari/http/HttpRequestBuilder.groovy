package fi.ayyy.portsari.http
import com.squareup.okhttp.FormEncodingBuilder
import com.squareup.okhttp.Request
import fi.ayyy.portsari.Portsari
import fi.ayyy.portsari.api.LocalPlayer

class HttpRequestBuilder {

    private def config = Portsari.getInstance().getConfig()

    /**
     * Base API URL, should omit the last slash.
     */
    private def baseUrl = config.getFormattedApiUrl()

    /**
     * API URL, should begin with a slash
     */
    private def url

    /**
     * Additional data to pass with the Request object
     */
    private def tag

    /**
     * True if the request needs the server token
     */
    private def needsToken = false

    /**
     * Params to send with the request
     */
    private def params = new FormEncodingBuilder()

    HttpRequestBuilder(String url) {
        this(url, false)
    }

    HttpRequestBuilder(String url, boolean needsToken) {
        this.url = url
        this.needsToken = needsToken
    }

    def add(String key, String value) {
        params.add(key, value)
        return this
    }

    def add(String key, Integer value) {
        params.add(key, String.valueOf(value))
        return this
    }

    def add(String key, UUID uuid) {
        params.add(key, uuid.toString())
        return this
    }

    def add(String key, LocalPlayer player) {
        return add(key, player.getUniqueId())
    }

    def tag(Object tag) {
        this.tag = tag
        return this
    }

    /**
     * Creates a POST request
     * @return A Request instance
     */
    def post() {
        if (needsToken) {
            params.add("token", config.getServerToken())
        }

        return new Request.Builder()
                .url(baseUrl + url)
                .tag(tag)
                .post(params.build())
                .build()
    }

    def get() {
        return new Request.Builder()
                .url(baseUrl + url)
                .get()
                .build()
    }
}
