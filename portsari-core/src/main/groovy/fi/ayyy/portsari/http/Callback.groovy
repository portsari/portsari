package fi.ayyy.portsari.http
import com.squareup.okhttp.Request
import com.squareup.okhttp.Response
import fi.ayyy.portsari.api.Actor
import groovy.transform.Canonical

import static fi.ayyy.portsari.util.localisation.Msg.UNEXPECTED_RESPONSE

@Canonical abstract class Callback implements com.squareup.okhttp.Callback {

    /**
     * The actor associated with this HTTP Request
     */
    Actor actor

    @Override void onFailure(Request request, IOException e) {
        e.printStackTrace()

        if (actor != null) {
            actor.print(UNEXPECTED_RESPONSE)
        }
    }

    abstract void onResponse(Response response) throws IOException

}
