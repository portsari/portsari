package fi.ayyy.portsari.command.exception

import fi.ayyy.portsari.command.Command
import fi.ayyy.portsari.util.localisation.Msg

class CommandException extends RuntimeException {

    Command command

    CommandException(String message) {
        super(message)
    }

    CommandException(Msg message) {
        super(message.f())
    }

    CommandException(String message, Throwable cause) {
        super(message, cause)
    }

    CommandException(Throwable cause) {
        super(cause)
    }

    CommandException(Msg message, Command command) {
        this(message.f(), command)
    }

    CommandException(String message, Command command) {
        super(message)
        this.command = command
    }
}
