package fi.ayyy.portsari.command
import fi.ayyy.portsari.api.Actor
import fi.ayyy.portsari.command.exception.*

import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method

import static fi.ayyy.portsari.util.localisation.Msg.COMMAND_NO_PERMISSIONS
import static fi.ayyy.portsari.util.localisation.Msg.COMMAND_PLAYER_ONLY

class CommandDispatcher {

    void call(Method method, CommandContext context, CommandInfo info) throws CommandException {
        Command command = info.getCommand()

        if (command.playerOnly() && !context.isPlayer()) {
            throw new WrongExecutorException(COMMAND_PLAYER_ONLY)
        }

        if (!isAuthorized(context.getActor(), command.permission())) {
            throw new AuthorizationException(COMMAND_NO_PERMISSIONS)
        }

        if (hasEnoughArgs(command.bounds(), context.getArgs())) {
            throw new ArgumentOutOfBoundsException("Args out of bounds", command)
        }

        try {
            method.invoke(info.getObject(), context)
        } catch (IllegalAccessException | InvocationTargetException e) {
            if (e.getCause() instanceof CommandException) {
                throw (CommandException) e.getCause()
            }

            throw new InvocationCommandException(e)
        }
    }

    /**
     * Checks if the args match given bounds
     *
     * @param bounds Bounds to check for
     * @param args Arguments to check for
     * @return True if within bounds
     */
    private boolean hasEnoughArgs(int[] bounds, String[] args) {
        (args.length < bounds[0] || (args.length > bounds[1] && bounds[1] >= 0))
    }

    /**
     * Checks if the Actor has the permissions for this command
     *
     * @param actor Actor to check for
     * @param permission Permission node
     * @return True if has permission
     */
    private boolean isAuthorized(Actor actor, String permission) {
        permission.isEmpty() || actor.hasPermission(permission)
    }
}
