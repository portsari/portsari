package fi.ayyy.portsari.command

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

@Retention(RetentionPolicy.RUNTIME)
@interface Command { // https://jira.codehaus.org/browse/GROOVY-3278 @ usage, desc, bounds

    /**
     * An array of possible aliases for this command.
     */
    String[] aliases()
    
    /**
     * The usage message, i.e. how the command should be used.
     */
    String usage() default "§cKäyttöohjetta ei saatavilla."
    
    /**
     * The permission required to execute this command.
     */
    String permission()
    
    /**
     * A description of what the command does.
     */
    String desc() default "§cApua ei saatavilla."
    
    /**
     * Can only players execute the command.
     */
    boolean playerOnly() default false
    
    /**
     * Minimum and maximum allowed argument lengths.
     * Index 0 is min number and index 1 is max number.
     * (set index 1 to -1 or lower to have unlimited arguments)
     * 
     */
    int[] bounds() default [ 0, 2147483647 ]
}