package fi.ayyy.portsari.command
import com.google.common.base.Optional
import fi.ayyy.portsari.Portsari
import fi.ayyy.portsari.api.Actor
import fi.ayyy.portsari.api.LocalPlayer
import fi.ayyy.portsari.command.exception.PlayerNotFoundException
import fi.ayyy.portsari.command.exception.WrongArgumentException
import fi.ayyy.portsari.util.localisation.Msg

import static fi.ayyy.portsari.util.localisation.Msg.ARG_MUST_BE_A_NUMBER
import static fi.ayyy.portsari.util.localisation.Msg.PLAYER_NOT_FOUND

class CommandContext {

    private Actor actor
    private String label
    private String[] args
    private List<Character> flags = new ArrayList()

    CommandContext(Actor actor, String label, String[] args) {
        this.actor = actor
        this.label = label

        def temp = args.toList()
        temp.removeAll {
            boolean isFlag = it.startsWith("-") && it.length() == 2 && !it.getAt(1).isNumber()
            if (isFlag) {
                flags.add(it.substring(1) as char)
            }

            isFlag
        }

        this.args = temp as String[]
    }

    void print(Msg msg) {
        print(msg.f())
    }

    /**
     * Sends a message to the Actor
     *
     * @param message Message to send
     */
    void print(String msg) {
        actor.print(msg)
    }

    /**
     * Gets the label of this command
     *
     * @return Something like 'test'
     */
    String getLabel() { label }

    /**
     * Gets all the arguments after the commands label. ie. if the command
     * label was test and the arguments were foo bar,
     * it would return 'foo bar'
     *
     * @return All the arguments entered.
     */
    String[] getArgs() { args }

    /**
     * Gets the argument at the specified index
     *
     * @param index The index to get
     * @return The string at the specified index
     */
    String getArg(int index) { args[index] }

    /**
     * Gets an integer at the specified index.
     *
     * @param index The index to get
     * @return The integer at the specified index.
     */
    Integer getInt(int index) {
        try {
            Integer.parseInt(args[index])
        } catch (NumberFormatException ignored) {
            throw new WrongArgumentException(ARG_MUST_BE_A_NUMBER)
        }
    }

   /**
    * Gets a string, starting from the specified index.
    * This should generally be the last argument
    *
    * @param index The index to start from
    * @param default If first index not found, default string to reply
    * @return The string starting from the specified index
    */
    Optional<String> getString(int index) {
        if (args.length - 1 < index) {
            return Optional.absent()
        }

        def builder = new StringBuilder()

        index.upto(args.length - 1, {
            builder.append(args[it]).append(" ")
        })

        return Optional.of(builder.toString().trim())
    }

    def hasFlag(String flag) { hasFlag(flag as char) }

    /**
     * Checks if the specified flag was entered
     *
     * @param flag Flag to check for
     * @return True if flag was entered
     */
    boolean hasFlag(char flag) { flags.contains(flag) }

    /**
     * Returns the length of the command arguments
     *
     * @return int length of args
     */
    int length() { args.length }

    /**
     * Checks if the sender is an instance of Player
     *
     * @return True if player
     */
    boolean isPlayer() { actor.isPlayer() }

    /**
     * Gets the Actor
     *
     * @return Actor who executed this command
     */
    Actor getActor() { actor }

    /**
     * Gets the Actor as a LocalPlayer
     *
     * @return LocalPlayer instance
     */
    LocalPlayer getActorAsPlayer() {
        return getPlayer(actor.getName())
    }

    LocalPlayer getPlayer(int index) throws PlayerNotFoundException {
        return getPlayer(getArg(index))
    }

    /**
     * Gets a LocalPlayer at the specified index
     *
     * @param index Index to look for Player
     * @return LocalPlayer instance
     * @throws PlayerNotFoundException
     */
    LocalPlayer getPlayer(String name) throws PlayerNotFoundException {
        Optional<LocalPlayer> player = Portsari.getInstance().getServer().findPlayer(name)

        if (player.isPresent()) {
            return player.get()
        }

        throw new PlayerNotFoundException(PLAYER_NOT_FOUND)
    }
}
