package fi.ayyy.portsari.command

import groovy.transform.Canonical

import java.lang.reflect.Method

@Canonical class CommandInfo {

    /**
     * The method which is used for this command
     */
    Method method

    /**
     * The object which holds this command
     */
    Object object

    /**
     * The Command which is used
     */
    Command command

}
