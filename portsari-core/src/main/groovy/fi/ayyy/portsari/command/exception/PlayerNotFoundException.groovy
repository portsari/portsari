package fi.ayyy.portsari.command.exception

import groovy.transform.InheritConstructors

@InheritConstructors
class PlayerNotFoundException extends CommandException {}
