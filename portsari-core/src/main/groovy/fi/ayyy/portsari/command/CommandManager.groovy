package fi.ayyy.portsari.command

import com.google.common.base.Optional
import com.google.common.eventbus.AllowConcurrentEvents
import com.google.common.eventbus.Subscribe
import fi.ayyy.portsari.Portsari
import fi.ayyy.portsari.api.event.CommandEvent
import fi.ayyy.portsari.command.exception.*
import fi.ayyy.portsari.commands.HuomautaCommand
import fi.ayyy.portsari.commands.HuomautuksetCommand
import fi.ayyy.portsari.commands.KumoaCommand
import fi.ayyy.portsari.commands.ReloadCommand

import java.lang.reflect.Method

import static fi.ayyy.portsari.util.localisation.Msg.*

class CommandManager {

    private Map<String, CommandInfo> commandMap = new HashMap()

    /**
     * The command prefix which is prepended to all help messages.
     */
    private def prefix

    private def dispatcher = new CommandDispatcher()

    CommandManager(Portsari portsari, String prefix) {
        this.prefix = prefix.trim()

        portsari.getEventBus().register(this)

        registerCommands(new HuomautaCommand())
        registerCommands(new KumoaCommand())
        registerCommands(new HuomautuksetCommand())
        registerCommands(new ReloadCommand())
    }

    private void registerCommands(Object obj) {
        for (Method method : obj.getClass().getMethods()) {
            Command command = method.getAnnotation(Command.class)

            // Confirm that @Command - annotation exists, and method has 1 argument, and that is CommandArgs
            if (command != null && method.getParameterTypes().length == 1 &&
                    method.getParameterTypes()[0] == CommandContext.class) {
                command.aliases().each { alias ->
                    registerCommand(alias, new CommandInfo(method, obj, command))
                }
            }
        }
    }

    private void registerCommand(String alias, CommandInfo info) {
        commandMap.put(alias.toLowerCase(), info)
    }

    @Subscribe
    @AllowConcurrentEvents
    void handleCommand(CommandEvent event) {
        CommandContext context = event.getContext()
        Optional<Method> command = getCommand(context)

        try {
            if (command.isPresent()) {
                dispatcher.call(command.get(), context, commandMap.get(context.getLabel()))
            } else {
                showHelp(context)
            }
        } catch (AuthorizationException e) {
            context.print(COMMAND_NO_PERMISSIONS)
        } catch (WrongExecutorException e) {
            context.print(COMMAND_PLAYER_ONLY)
        } catch (ArgumentOutOfBoundsException e) {
            Command cmd = e.getCommand()
            context.print(COMMAND_ARGUMENT_OUT_OF_BOUNDS.f(prefix, cmd.aliases()[0], cmd.usage(), cmd.desc()))
        } catch (WrongArgumentException e) {
            context.print(e.getMessage())
        } catch (PlayerNotFoundException e) {
            context.print(PLAYER_NOT_FOUND)
        } catch (CommandException e) {
            context.print(INTERNAL_ERROR)
            e.printStackTrace()
        }
    }

    private Optional<Method> getCommand(CommandContext context) {
        Optional.fromNullable(commandMap.get(context.getLabel())?.getMethod())
    }

    private void showHelp(CommandContext context) {
        commandMap.keySet().each { label ->
            Command command = commandMap.get(label).getMethod().getAnnotation(Command.class)

            context.print(COMMAND_USAGE.f(prefix, command.aliases()[0], command.usage(), command.desc()))
        }
    }
}
