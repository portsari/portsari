package fi.ayyy.portsari.command.exception

import groovy.transform.InheritConstructors

@InheritConstructors
class InvocationCommandException extends CommandException {}
