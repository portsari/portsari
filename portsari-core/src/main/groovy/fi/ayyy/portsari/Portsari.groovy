package fi.ayyy.portsari
import com.google.common.eventbus.AsyncEventBus
import com.google.common.eventbus.EventBus
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.squareup.okhttp.Callback
import com.squareup.okhttp.OkHttpClient
import com.squareup.okhttp.Request
import com.squareup.okhttp.Response
import fi.ayyy.portsari.api.LocalConfiguration
import fi.ayyy.portsari.api.LocalPlayer
import fi.ayyy.portsari.api.ServerInterface
import fi.ayyy.portsari.api.event.PlayerLoginEvent
import fi.ayyy.portsari.command.CommandManager
import fi.ayyy.portsari.http.HttpRequestBuilder
import fi.ayyy.portsari.http.InvalidResponseInterceptor
import groovy.util.logging.Log

import java.util.concurrent.Executors

import static fi.ayyy.portsari.util.localisation.Msg.*

@Log
class Portsari {

    private static final Portsari instance = new Portsari()

    final EventBus eventBus = new AsyncEventBus(Executors.newCachedThreadPool())
    final PlatformManager platformManager = new PlatformManager(this)
    final CommandManager commandManager = new CommandManager(this, "/pr")

    final OkHttpClient http = new OkHttpClient()
    static final Gson GSON = new GsonBuilder().setDateFormat("yyyy-mm-dd HH:mm:ss").create()

    /**
     * True if the server has a valid auth token
     */
    static isAuthorized = false

    private Portsari() {
        getHttp().interceptors().add(new InvalidResponseInterceptor())
        getEventBus().register(new EventListener(this))
    }

    static Portsari getInstance() { instance }

    void validateServerToken() {
        isAuthorized = true
        log.info(VALIDATE_INFO.f())

        def params = new HttpRequestBuilder("/server/validate", true).post()
        getHttp().newCall(params).enqueue(new Callback() {
            @Override void onFailure(Request request, IOException e) {
                isAuthorized = false
                log.severe(VALIDATE_ERROR.f())
                e.printStackTrace()
            }

            @Override void onResponse(Response response) throws IOException {
                if (response.code() == 200) {
                    log.info(VALIDATE_SUCCESS.f())
                } else {
                    log.severe(VALIDATE_FAILED.f())
                }
            }
        })
    }

    ServerInterface getServer() { getPlatformManager().getServer() }

    LocalConfiguration getConfig() { getServer().getConfiguration() }

    void handleLogin(LocalPlayer player) {
        PlayerLoginEvent event = new PlayerLoginEvent(player)
        getEventBus().post(event)
    }
}
