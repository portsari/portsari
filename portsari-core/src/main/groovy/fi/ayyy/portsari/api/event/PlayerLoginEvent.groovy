package fi.ayyy.portsari.api.event

import fi.ayyy.portsari.api.LocalPlayer
import groovy.transform.Canonical

@Canonical class PlayerLoginEvent  {

    /**
     * The LocalPlayer who just logged in
     */
    LocalPlayer player

}
