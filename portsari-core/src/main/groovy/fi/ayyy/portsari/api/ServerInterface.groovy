package fi.ayyy.portsari.api

import com.google.common.base.Optional

interface ServerInterface {

    /**
     * Gets the Console of this server
     *
     * @return Console Actor
     */
    Actor getConsole()

    /**
     * Gets a LocalPlayer from the server
     *
     * @param name Name of the player
     * @return LocalPlayer
     */
    Optional<LocalPlayer> findPlayer(String name)

    /**
     * Disables the plugin
     */
    void disable()

    /**
     * Reloads the plugins configuration
     */
    void reload()

    /**
     * Gets the servers implementation of the configuration
     *
     * @return the configuration
     */
    LocalConfiguration getConfiguration()

    /**
     * Checks if the server is set to online-mode
     *
     * @return True if online
     */
    boolean isOnlineMode()

    /**
     * Gets the server platforms name, eg. Bukkit
     *
     * @return Server name
     */
    String getServerName()

    /**
     * Gets the servers version, eg. 1.7.5
     *
     * @return Server version
     */
    String getServerVersion()

    /**
     * Sends a message to everyone on the server
     *
     * @param msg Message to send
     */
    void broadcast(String msg)

    /**
     * Sends a message to everyone who has the specified permission
     *
     * @param msg Message to send
     * @param permission Required permission
     */
    void broadcast(String msg, String permission)

}
