package fi.ayyy.portsari.api

abstract class LocalConfiguration {

    /**
     * Server token for the API
     */
    String SERVER_TOKEN

    /**
     * API version to use
     */
    Integer API_VERSION = 1

    /**
     * API URL. <version> is changed to API_VERSION
     */
    String API_URL = "https://portsari.fi/api/<version>"

    List<Integer> IGNORED_REASONS

    /**
     * Loads the configuration
     */
    abstract void load()

    def getServerToken() { SERVER_TOKEN }

    def getAPIVersion() { API_VERSION }

    def getAPIUrl() { API_URL }

    def getFormattedApiUrl() {
        getAPIUrl().replace("<version>", Integer.toString(getAPIVersion()))
    }

    def getIgnoredReasons() { IGNORED_REASONS }

}
