package fi.ayyy.portsari.api

import fi.ayyy.portsari.util.localisation.Msg

interface Actor {

    /**
     * Sends a message to this Actor
     *
     * @param msg Message to send
     */
    void print(String msg)

    /**
     * Sends a message to this Actor
     *
     * @param msg Message to send
     */
    void print(Msg msg)

    /**
     * Checks if the Actor is a player
     *
     * @return True if player
     */
    boolean isPlayer()

    /**
     * Gets the name of this actor
     *
     * @return the name
     */
    String getName()

    /**
     * Checks if this actor has the specified permission.
     *
     * @param permission the permission
     * @return True if has permission, or is Console
     */
    boolean hasPermission(String permission)

}
