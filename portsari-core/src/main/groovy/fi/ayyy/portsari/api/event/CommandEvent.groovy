package fi.ayyy.portsari.api.event

import fi.ayyy.portsari.api.Actor
import fi.ayyy.portsari.command.CommandContext

class CommandEvent {

    /**
     * The CommandContext associated with this command
     */
    final CommandContext context

    CommandEvent(Actor actor, String label, String[] args) {
        this(new CommandContext(actor, label.toLowerCase(), args))
    }

    CommandEvent(CommandContext context) {
        this.context = context
    }
}
