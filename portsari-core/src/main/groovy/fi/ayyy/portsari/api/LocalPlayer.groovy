package fi.ayyy.portsari.api

import fi.ayyy.portsari.util.localisation.Msg

interface LocalPlayer {

    /**
     * Sends a message to this player
     *
     * @param msg Message  to send
     */
    void print(String msg)

    /**
     * Sends a message to this Actor
     *
     * @param msg Message to send
     */
    void print(Msg msg)

    /**
     * Gets this players Unique Id
     *
     * @return UUID
     */
    UUID getUniqueId()

    /**
     * Gets the name of this player
     *
     * @return the name
     */
    String getName()

    /**
     * Checks if this player has the specified permission.
     *
     * @param permission the permission
     * @return True if has permission
     */
    boolean hasPermission(String permission)

    /**
     * Checks if this player is a server operator
     *
     * @return True if Op
     */
    boolean isOp()

    /**
     * Checks if this player is online currently
     *
     * @return True if online
     */
    boolean isOnline()

}
