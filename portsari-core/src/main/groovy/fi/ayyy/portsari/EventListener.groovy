package fi.ayyy.portsari
import com.google.common.eventbus.AllowConcurrentEvents
import com.google.common.eventbus.Subscribe
import com.squareup.okhttp.Response
import fi.ayyy.portsari.api.event.PlayerLoginEvent
import fi.ayyy.portsari.gson.Reports
import fi.ayyy.portsari.http.Callback
import fi.ayyy.portsari.http.HttpRequestBuilder
import groovy.transform.Canonical

import static fi.ayyy.portsari.util.localisation.Msg.REPORT_ON_LOGIN

@Canonical class EventListener {

    Portsari portsari

    @Subscribe
    @AllowConcurrentEvents
    void onPlayerLogin(PlayerLoginEvent event) {
        def player = event.getPlayer()

        if (player.hasPermission("portsari.trigger") && portsari.getIsAuthorized()) {
            def request = new HttpRequestBuilder("/reports/get", true)
                    .add("uuid", player.getUniqueId())
                    .post()

            portsari.getHttp().newCall(request).enqueue(new Callback() {
                @Override void onResponse(Response response) throws IOException {
                    if (response.code() == 200) {
                        def reports = portsari.GSON.fromJson(response.body().source().readUtf8(), Reports.class)
                        reports.removeIgnored()

                        if (reports.hasReports()) {
                            portsari.getServer().broadcast(REPORT_ON_LOGIN.f(player.getName(), reports.count()), "portsari.notify")
                        }
                    }
                }
            })
        }
    }
}