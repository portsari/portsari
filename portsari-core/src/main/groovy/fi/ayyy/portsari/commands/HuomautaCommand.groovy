package fi.ayyy.portsari.commands
import com.squareup.okhttp.Response
import fi.ayyy.portsari.Portsari
import fi.ayyy.portsari.command.Command
import fi.ayyy.portsari.command.CommandContext
import fi.ayyy.portsari.gson.Report
import fi.ayyy.portsari.http.Callback
import fi.ayyy.portsari.http.HttpRequestBuilder

import static fi.ayyy.portsari.util.localisation.Msg.REPORT_SUCCESS

class HuomautaCommand {

    @Command(aliases="huomauta", permission="portsari.huomauta", bounds=[2, -1],
            usage="<pelaaja> <syy> [lisätietoja]", desc="Antaa huomautuksen kyseiselle pelaajalle.", playerOnly=true)
    ilmianna(CommandContext context) {
        def request = new HttpRequestBuilder("/reports/new", true)
                .add("reporter", context.getActorAsPlayer())
                .add("player", context.getPlayer(0))
                .add("reason", context.getArg(1))
                .add("info", context.getString(2).or("Ei lisätietoja saatavilla"))
                .tag(context)
                .post()

        Portsari.getInstance().getHttp().newCall(request).enqueue(new Callback(context.getActor()) {
            @Override void onResponse(Response response) throws IOException {
                if (response.code() == 201) {
                    def report = Portsari.GSON.fromJson(response.body().string(), Report.class)
                    def player = context.getPlayer(0)

                    context.print(REPORT_SUCCESS.f(player.getName(), player.getUniqueId(), report.id))
                }
            }
        })
    }
}
