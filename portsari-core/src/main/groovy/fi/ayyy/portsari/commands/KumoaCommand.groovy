package fi.ayyy.portsari.commands
import com.squareup.okhttp.Response
import fi.ayyy.portsari.Portsari
import fi.ayyy.portsari.command.Command
import fi.ayyy.portsari.command.CommandContext
import fi.ayyy.portsari.http.Callback
import fi.ayyy.portsari.http.HttpRequestBuilder

import static fi.ayyy.portsari.util.localisation.Msg.REPORT_DELETED

class KumoaCommand {

    @Command(aliases="kumoa", permission="portsari.kumoa", bounds=[1, 1],
            usage="<id>", desc="Kumoaa kyseisen huomautuksen.", playerOnly=true)
    kumoa(CommandContext context) {
        def request = new HttpRequestBuilder("/reports/remove", true)
                .add("uuid", context.getActorAsPlayer())
                .add("id", context.getInt(0))
                .tag(context)
                .post()

        Portsari.getInstance().getHttp().newCall(request).enqueue(new Callback(context.getActor()) {
            @Override void onResponse(Response response) throws IOException {
                if (response.code() == 204) {
                    context.print(REPORT_DELETED)
                }
            }
        })
    }
}
