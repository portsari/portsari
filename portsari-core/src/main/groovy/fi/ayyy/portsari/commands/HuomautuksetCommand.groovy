package fi.ayyy.portsari.commands
import com.squareup.okhttp.Response
import fi.ayyy.portsari.Portsari
import fi.ayyy.portsari.command.Command
import fi.ayyy.portsari.command.CommandContext
import fi.ayyy.portsari.gson.Reports
import fi.ayyy.portsari.http.Callback
import fi.ayyy.portsari.http.HttpRequestBuilder

import static fi.ayyy.portsari.util.localisation.Msg.*

class HuomautuksetCommand {

    @Command(aliases="huomautukset", permission="portsari.huomautukset", bounds=[0, 1],
            usage="[-a] [pelaaja]", desc="Näyttää kyseisen pelaajan, tai sinun huomautukset.")
    varoitukset(CommandContext context) {
        if (!context.isPlayer() && context.length() != 1) {
            context.print(SPECIFY_PLAYER); return
        }

        // Check if Player argument has been entered and sender is a player
        def player = (context.isPlayer() && context.length() == 0) ? context.getActorAsPlayer() : context.getPlayer(0)

        def request = new HttpRequestBuilder("/reports/get", true)
                .add("uuid", player)
                .tag(context)
                .post()

        Portsari.getInstance().getHttp().newCall(request).enqueue(new Callback(context.getActor()) {
            @Override void onResponse(Response response) throws IOException {
                if (response.code() == 200) {
                    def reports = Portsari.GSON.fromJson(response.body().string(), Reports.class)

                    if (!context.hasFlag('a')) reports.removeIgnored()
                    if (reports.hasReports()) {
                        reports.getReports().each { report ->
                            context.print(REPORT_ROW.f(report.id, report.reason, report.server.name, report.info))
                        }
                    } else {
                        context.print(REPORT_NO_RESULTS.f(player.getName()))
                    }
                }
            }
        })
    }
}
