package fi.ayyy.portsari.commands

import fi.ayyy.portsari.Portsari
import fi.ayyy.portsari.command.Command
import fi.ayyy.portsari.command.CommandContext

import static fi.ayyy.portsari.util.localisation.Msg.CONFIG_RELOADED

class ReloadCommand {

    @Command(aliases="reload", permission="portsari.reload",
            usage="", desc="Lataa konfiguraation uusiksi ja varmistaa tunnistusavaimen uudelleen.")
    reload(CommandContext context) {
        Portsari.getInstance().getServer().reload()
        Portsari.getInstance().validateServerToken()

        context.print(CONFIG_RELOADED)
    }
}
