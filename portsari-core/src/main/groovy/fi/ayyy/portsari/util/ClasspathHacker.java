package fi.ayyy.portsari.util;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

public class ClasspathHacker {

    /**
     * Adds a file to the classpath
     *
     * @param file the file to be added
     * @throws IOException
     */
    public static void addFile(File file) throws IOException {
        addURL(file.toURI().toURL());
    }

    /**
     * Adds the content pointed by the URL to the classpath.
     *
     * @param url the URL pointing to the content to be added
     * @throws IOException
     */
    public static void addURL(URL url) throws IOException {
        URLClassLoader sysloader = (URLClassLoader) ClassLoader.getSystemClassLoader();
        Class<?> sysclass = URLClassLoader.class;

        try {
            Method method = sysclass.getDeclaredMethod("addURL", URL.class);
            method.setAccessible(true);
            method.invoke(sysloader, url);
        } catch (Throwable t) {
            t.printStackTrace();
            throw new IOException("Error, could not add URL to system ClassLoader");
        }
    }
}
