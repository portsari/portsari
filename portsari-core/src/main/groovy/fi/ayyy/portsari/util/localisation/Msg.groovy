package fi.ayyy.portsari.util.localisation

enum Msg {
    
    UNEXPECTED_RESPONSE ("§4Odottamaton vastaus. Katso konsolista lisäinformaatiota."),

    REPORT_ON_LOGIN ("§3Pelaaja §6{0} §3on saanut §6{1} §3huomautusta §e3 kuukauden §3aikana."),
    REPORT_NO_RESULTS ("§3Pelaajasta §6{0} §3ei löytynyt huomautuksia."),
    REPORT_ROW ("§6#{0}§c§l> §3{1} §6@ §3{2}§c, §e{3}"),
    REPORT_SUCCESS ("§3Luotiin huomautus pelaajasta §6{0}\n §8(§e§o{1}§r§8)\n" +
                    "§3Jos huomautus oli virheellinen, voit kumota sen komennolla §6/pr kumoa {2}§3."),
    REPORT_ERROR_CREATING ("§4Huomautuksen luomisessa tapahtui virhe, katso konsolista lisäinformaatiota."),
    REPORT_DELETED ("§3Huomautus poistettiin §aonnistuneesti!"),
    REPORT_ERROR ("§4Tapahtui virhe, yritä myöhemmin uudelleen."),

    COMMAND_USAGE ("§6{0} {1} {2}§8§l: §e{3}"),
    COMMAND_ARGUMENT_OUT_OF_BOUNDS ("§4Argumentti ei ole sallitulla välillä, käyttö: " + COMMAND_USAGE),
    COMMAND_NO_PERMISSIONS ("§4Sinulla ei ole tarvittavia oikeuksia tähän komentoon!"),
    COMMAND_PLAYER_ONLY ("§4Komentoa ei voida suorittaa konsolista!"),

    VALIDATE_SUCCESS ("§3Onnistui! Palvelimen tunnisteavain on §avalidi!"),
    VALIDATE_FAILED ("§4Virheellinen palvelimen tunnisteavain. Et voi käyttää kaikkia Portsarin ominaisuuksia ilman toimivaa tunnistusavainta."),
    VALIDATE_ERROR ("§4Palvelimen tunnisteavaimen varmistamisessa tapahtui virhe. Yritä hetken kuluttua uudelleen komennolla /pr reload"),
    VALIDATE_IS_DISABLED ("§4Portsari ei toimi ilman voimassaolevaa tunnistusavainta."),
    VALIDATE_OFFLINE_MODE ("§4Palvelin ei ole online-tilassa, Portsaria ei voida käyttää offline-tilassa."),
    VALIDATE_INFO ("§3Varmistetaan palvelimen tunnisteavainta."),

    PLAYER_NOT_FOUND ("§4Määriteltyä pelaajaa ei löytynyt."),
    SPECIFY_PLAYER ("§4Määrittele pelaaja."),
    ARG_MUST_BE_A_NUMBER ("§4Argumentin tulee olla numero."),

    INTERNAL_ERROR_COMMAND ("§4Tapahtui sisäinen virhe. Määriteltyä komentoa ei löytynyt."),
    INTERNAL_ERROR ("§4Tapahtui sisäinen virhe. Katso konsolista lisäinformaatiota."),

    CONFIG_RELOADED ("§aKonfiguraatio ladattu uudelleen.")
    
    private def key
    
    Msg(String key) {
        this.key = key
    }

    /**
     * Formats the specified string
     *
     * @param args Args to replace placeholders with
     * @return A formatted string
     */
    def f(Object... args) {
        def message = toString()

        args.eachWithIndex { def entry, int i ->
            message = message.replace("{" + i + "}", String.valueOf(entry))
        }

        return message
    }

    def f() { toString() }

    String toString() { this.key }

}
