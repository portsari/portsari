package fi.ayyy.portsari
import fi.ayyy.portsari.api.ServerInterface
import groovy.transform.Canonical
import groovy.util.logging.Log

import static com.google.common.base.Preconditions.checkNotNull
import static fi.ayyy.portsari.util.localisation.Msg.VALIDATE_OFFLINE_MODE

@Log
@Canonical class PlatformManager {

    Portsari portsari
    ServerInterface server

    boolean register(ServerInterface server) {
        checkNotNull(server)

        if (this.server) {
            throw new RuntimeException("There can only be one platform simultaneously")
        }

        this.server = server
        if (server.isOnlineMode()) {
            portsari.validateServerToken()
        } else {
            log.severe(VALIDATE_OFFLINE_MODE.f())

            unregister()
            server.disable()
        }

        server.isOnlineMode()
    }

    void unregister() {
        this.server = null
    }
}
